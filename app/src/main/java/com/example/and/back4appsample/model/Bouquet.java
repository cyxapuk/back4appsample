package com.example.and.back4appsample.model;

import com.j256.ormlite.field.DatabaseField;

public class Bouquet {

    @DatabaseField(id=true)
    public String id ;
    @DatabaseField
    public int categoryId;
    @DatabaseField
    public String bouquetName;
    @DatabaseField
    public String picturePath;
    @DatabaseField
    public String description;
    @DatabaseField
    public int price;

    public Bouquet(){}

    public String getId() {
        return id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getBouquetName() {
        return bouquetName;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setBouquetName(String bouquetName) {
        this.bouquetName = bouquetName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
