package com.example.and.back4appsample.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "myTable")
public class NewDBObject {

    @DatabaseField (id = true, generatedId = true)
    public int id_product;
    @DatabaseField()
    public String cost;
    @DatabaseField()
    public String description;
    @DatabaseField()
    public String name;
    @DatabaseField()
    public String picture;

    public NewDBObject(){}

    public NewDBObject(String cost, String description, String name, String picture) {
        this.cost = cost;
        this.description = description;
        this.name = name;
        this.picture = picture;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
