package com.example.and.back4appsample.dbase;

import com.example.and.back4appsample.model.Bouquet;
import com.example.and.back4appsample.model.NewDBObject;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTableConfig;

import java.sql.SQLException;
import java.util.List;

public class NewObjectDAO extends BaseDaoImpl<NewDBObject, Integer>{

    public NewObjectDAO(ConnectionSource connectionSource, Class<NewDBObject> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public int addItem(NewDBObject item) throws SQLException{

        String statement = "INSERT OR REPLACE INTO myTable (" +
                "cost, " +
                "description, " +
                "name, " +
                "picture) VALUES ";

        StringBuilder builder = new StringBuilder(statement);

        builder.append(" ").append(item.cost).append(",")
                .append(" \"").append(item.description).append("\",")
                .append(" \"").append(item.name).append("\",")
                .append(" \"").append(item.picture).append(") ");

        return  updateRaw(builder.toString());
    }

    public List<NewDBObject> getItemByName(String name) throws SQLException{
        PreparedQuery query  = getPreparedQuery(name);
        return query(query);
    }

    private PreparedQuery getPreparedQuery(String name)throws SQLException{
        return queryBuilder()
                .orderBy("id_product",true)
                .groupBy("name")
                .where()

                .like("name", name)
                
                .prepare();
    }
}
