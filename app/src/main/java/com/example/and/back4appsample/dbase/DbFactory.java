package com.example.and.back4appsample.dbase;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

public class DbFactory {

    private static DbHelper helper;
    private static DbFactory instance;

    public static DbFactory getInstance() {
        if (instance == null) {
            synchronized (DbFactory.class) {
                if (instance == null) {
                    instance = new DbFactory();
                }
            }
        }
        return instance;
    }

    public static void init(Context context) {
        if (helper == null)
            helper = OpenHelperManager.getHelper(context, DbHelper.class);
    }

    public void release() {
        if (helper != null) {
            OpenHelperManager.releaseHelper();
            helper = null;
        }
    }

    public DbHelper getHelper() {
        return helper;
    }
}
