package com.example.and.back4appsample.core;

/**
 * Created by and on 01.03.2017.
 */

public class CardItem {
    private String url ;
    private String name ;
    private  int price ;

    public CardItem() {
        this.url = "";
        this.name = "";
        this.price = -1;
    }

    public CardItem(String url, String name, int price) {
        this.url = url;
        this.name = name;
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
