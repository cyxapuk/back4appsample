package com.example.and.back4appsample.model;

public class Enums {

    public enum FlowerType{
        Rose (0, "Розы", "http://ogorodsadovod.com/sites/default/files/u79/2015/11/rose1_0.jpg"),
        Tulip (1, "Тюльпаны", "https://posevnaya.com/wp-content/uploads/2016/10/%D0%A2%D1%8E%D0%BB%D1%8C%D0%BF%D0%B0%D0%BD%D1%8B.jpg"),
        Hydrangea (2, "Гортензии", "https://posevnaya.com/wp-content/uploads/2016/10/%D0%A2%D1%8E%D0%BB%D1%8C%D0%BF%D0%B0%D0%BD%D1%8B.jpg"),
        Eustom (3, "Эустомы", "http://www.odelita.ru/sites/default/files/styles/800_600/public/images/2016/01/eustoma-530.jpg?itok=xC-iV8YJ");

        private final String strName;
        private final String strImgRef;
        private final int CategoryId;

        FlowerType(int CategoryId, String strName, String strImgRef) {
            this.strName = strName;
            this.strImgRef = strImgRef;
            this.CategoryId = CategoryId;
        }

        public String getName (){
            return strName;
        }

        public String getRef (){
            return strImgRef;
        }

        public int getCategory(){
            return CategoryId;
        }

        @Override
        public String toString() {
            return strName;
        }
    }
}
