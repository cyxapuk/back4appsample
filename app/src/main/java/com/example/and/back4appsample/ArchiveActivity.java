package com.example.and.back4appsample;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.and.back4appsample.adapters.ResultAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class ArchiveActivity extends BaseActivity {

    @Bind(R.id.results) ListView lvResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ResultAdapter adapter = new ResultAdapter(this, new ArrayList<sss>());
        lvResults.setAdapter(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Мои заказы");
    }

    @OnClick(R.id.btn_update)
    public void onClickUpdate(View v) {
        createRequestMale();
    }

    @OnClick(R.id.btn_updateAll)
    public void onClickUpdateAll(View v) {
        createRequestAll();
    }

    private void createRequestMale(){
        ParseQuery<sss> query = ParseQuery.getQuery(sss.class);
        query.whereEqualTo("gender", true);
        query.findInBackground(new FindCallback<sss>() {
            @Override
            public void done(List<sss> objects, ParseException e) {
                if (e != null){
                    Toast.makeText(ArchiveActivity.this, "Error -> "+e.getMessage(),Toast.LENGTH_SHORT).show();
                    Log.e("Look","Error"+e.getLocalizedMessage());
                    e.printStackTrace();
                }
                if(objects.size() > 0){
                    ResultAdapter adapter = ((ResultAdapter)lvResults.getAdapter());
                    adapter.getArrayList().clear();
                    adapter.getArrayList().addAll(objects);
                    adapter.notifyDataSetChanged();

                }
            }
        });
    }

    private void createRequestAll(){
        ParseQuery<sss> query = ParseQuery.getQuery(sss.class);
        query.findInBackground(new FindCallback<sss>() {
            @Override
            public void done(List<sss> objects, ParseException e) {
                if (e != null){
                    Toast.makeText(ArchiveActivity.this, "Error -> "+e.getMessage(),Toast.LENGTH_SHORT).show();
                    Log.e("Look","Error"+e.getLocalizedMessage());
                    e.printStackTrace();
                }
                if(objects.size() > 0){
                    ResultAdapter adapter = ((ResultAdapter)lvResults.getAdapter());
                    adapter.getArrayList().clear();
                    adapter.getArrayList().addAll(objects);
                    adapter.notifyDataSetChanged();

                }
            }
        });
    }
}
