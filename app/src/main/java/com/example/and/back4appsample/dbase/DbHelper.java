package com.example.and.back4appsample.dbase;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.and.back4appsample.model.Bouquet;
import com.example.and.back4appsample.model.NewDBObject;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DbHelper extends OrmLiteSqliteOpenHelper{

    private static final String DB_NAME = "flower_delivery.db";
    private static DbHelper instance;
    private static String TAG = "DbHelperTag";
    private BouquetDao bouquet;
    private NewObjectDAO object;


    private DbHelper (Context context){
        super(context, DB_NAME, null, getAppVersionCode(context));
    }

    public static synchronized DbHelper getHelper(Context context) {
        if (instance == null)
            instance = new DbHelper(context);
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try{
            TableUtils.createTable(connectionSource, Bouquet.class);
            TableUtils.createTable(connectionSource, NewDBObject.class);    // создание таблицы
        }catch (SQLException e ){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        // No upgrade
    }

    public BouquetDao getBouquetDao () throws SQLException {
        if(bouquet == null){
                bouquet = new BouquetDao(getConnectionSource(), Bouquet.class);
            }
        return bouquet;
    }

    //Для каждой таблицы нужен класс Dao через который будут вестись все операции с таблицей
    public NewObjectDAO getNewObjectDao() throws SQLException{
        if(object == null){
            object = new NewObjectDAO(getConnectionSource(), NewDBObject.class);
        }
        return object;
    }


    private static int getAppVersionCode(Context context) {
        int version = 0;
        try {
            version = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }
}
