package com.example.and.back4appsample.core;

import android.app.Application;

import com.example.and.back4appsample.R;
import com.example.and.back4appsample.sss;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ParseObject.registerSubclass(sss.class);
        Parse.initialize(new Parse.Configuration.Builder(this)
                .clientKey(getString(R.string.client_key))
                .applicationId(getString(R.string.application_id))
                .server(getString(R.string.back4app_server))
                .build());
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }
}
