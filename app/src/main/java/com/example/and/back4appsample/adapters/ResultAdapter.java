package com.example.and.back4appsample.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.and.back4appsample.R;
import com.example.and.back4appsample.sss;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by and on 01.03.2017.
 */

public class ResultAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<sss> results;

    public ResultAdapter (Context context, ArrayList<sss> results){
        this.results = results;
        this.context = context;
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public sss getItem(int position) {
        return results.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = (View) inflater.inflate(R.layout.item_result, parent, false);
        }
        sss item = (sss)getItem(position);
        ((TextView)convertView.findViewById(R.id.tv_name)).setText(item.getName());
        ((TextView)convertView.findViewById(R.id.tv_num)).setText(" "+item.getNum());
        ((TextView)convertView.findViewById(R.id.tv_gender)).setText(getGender(item.getGender()));
        ((TextView)convertView.findViewById(R.id.tv_created)).setText(getDate(item.getCreatedAt()));
        ((TextView)convertView.findViewById(R.id.tv_age)).setText(getDate(item.getAge()));

        return convertView;
    }

    private String getGender(boolean gender){
        if(gender)
            return "male";
        return "female";
    }

    private String getDate (Date date){
        SimpleDateFormat simpleDate =  new SimpleDateFormat("dd-MM-yyyy");
        return simpleDate.format(date);
    }

    public List getArrayList(){
        return results;
    }
}
