package com.example.and.back4appsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.example.and.back4appsample.adapters.BasketAdapter;
import com.example.and.back4appsample.model.Bouquet;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class CatalogActivity extends BaseActivity {

    @Bind(R.id.card_list) RecyclerView cardList;
    List<Bouquet> bouquets ;

    private BasketAdapter adapter;

    @Override
    protected int getContentResId() {
        return R.layout.activity_card_list;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cardList.setLayoutManager(new LinearLayoutManager(this));
        bouquets = getAllBouquets();
        adapter = new BasketAdapter(bouquets, this);
        adapter.setClickListener(new BasketAdapter.ActionListener() {
            @Override
            public void onCardClick(int adapterPosition) {
                Intent intent = new Intent(CatalogActivity.this, ArchiveActivity.class);
                startActivity(intent);
            }

            @Override
            public void onDeleteClick(int adapterPosion) {
                Bouquet deleteItem = adapter.getItem(adapterPosion);
                try {
                    mDbHelper.getBouquetDao().delete(deleteItem);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                bouquets.remove(adapterPosion);
                adapter.notifyItemRemoved(adapterPosion);
            }
        });
        cardList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Каталог");
    }

    private List<Bouquet> getAllBouquets(){
        try {
            return mDbHelper.getBouquetDao().getAllBouquets();
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }


    //Реакция на полную очистку списка
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        super.onNavigationItemSelected(item);

        int count = adapter.getItemCount();
        bouquets.clear();
        adapter.notifyItemRangeRemoved(0,count);
        return true;
    }
}






//    private void generateCards(){
//        CardItem card = new CardItem();
//        card.setName("Розы");
//        card.setPrice(50);
//        card.setUrl("http://ogorodsadovod.com/sites/default/files/u79/2015/11/rose1_0.jpg");
//        cards.add(card);
//
//        CardItem card1 = new CardItem();
//        card1.setName("Тюльпаны");
//        card1.setPrice(30);
//        card1.setUrl("https://posevnaya.com/wp-content/uploads/2016/10/%D0%A2%D1%8E%D0%BB%D1%8C%D0%BF%D0%B0%D0%BD%D1%8B.jpg");
//        cards.add(card1);
//
//        CardItem card2 = new CardItem();
//        card2.setName("Гортензии");
//        card2.setPrice(150);
//        card2.setUrl("http://udivitelno.com/images/10/gortenziya-sadovaya/2-%D0%A0%D0%B0%D0%B7%D0%BD%D0%BE%D1%86%D0%B2%D0%B5%D1%82%D0%BD%D1%8B%D0%B5%20%D0%B3%D0%BE%D1%80%D1%82%D0%B5%D0%BD%D0%B7%D0%B8%D0%B8%20%D0%B2%20%D1%81%D0%B0%D0%B4%D1%83.jpg");
//        cards.add(card2);
//
//        CardItem card3 = new CardItem();
//        card3.setName("Эустомы");
//        card3.setPrice(70);
//        card3.setUrl("http://www.odelita.ru/sites/default/files/styles/800_600/public/images/2016/01/eustoma-530.jpg?itok=xC-iV8YJ");
//        cards.add(card3);

//    }