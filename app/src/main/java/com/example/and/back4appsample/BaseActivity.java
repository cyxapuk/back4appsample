package com.example.and.back4appsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.and.back4appsample.dbase.DbHelper;

import java.sql.SQLException;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    protected DbHelper mDbHelper ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentResId());
        ButterKnife.bind(this);
        initDrawer();
        mDbHelper = DbHelper.getHelper(this);
    }

    private void initDrawer(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    protected abstract int getContentResId();

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_archive:
                if(!(this instanceof ArchiveActivity)){
                    Intent intent = new Intent(this, ArchiveActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.nav_catalog:
                if(!(this instanceof CatalogActivity)){
                    Intent intent = new Intent(this, CatalogActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.nav_case:
                if(!(this instanceof CatalogActivity)){
                    Intent intent = new Intent(this, CatalogActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.nav_remove_all:
                try {
                    mDbHelper.getBouquetDao().removeAll();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.nav_add_bouquet:
                if(!(this instanceof AddBouquetActivity)){
                    Intent intent = new Intent(this, AddBouquetActivity.class);
                    startActivity(intent);
                }
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else
            super.onBackPressed();
    }
}
