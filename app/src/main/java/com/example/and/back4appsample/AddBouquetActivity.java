package com.example.and.back4appsample;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.and.back4appsample.model.Bouquet;
import com.example.and.back4appsample.model.Enums;

import java.sql.SQLException;
import java.util.Random;

import butterknife.Bind;
import butterknife.OnClick;

public class AddBouquetActivity extends BaseActivity{

    @Bind(R.id.spin_bouquet_type)
    Spinner typeChooser;
    @Bind(R.id.et_bouquet_name)
    EditText etBouquetName;
    @Bind(R.id.et_description)
    EditText etDescription;

    @Override
    protected int getContentResId() {
        return R.layout.activity_add_bouquet;
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Создайте букет");
        typeChooser.setAdapter(new ArrayAdapter<Enums.FlowerType>(this, android.R.layout.simple_spinner_item, Enums.FlowerType.values()));
        typeChooser.setSelection(0);
    }

    @OnClick(R.id.btn_add_bouquet)
    public void onAddBouquetClick(){
        Bouquet bouquet = new Bouquet();
        String name = etBouquetName.getText().toString();
        if(name.isEmpty()){
            Toast.makeText(this, "Введите название букета", Toast.LENGTH_SHORT).show();
            return;
        }
        bouquet.setBouquetName(name);
        Enums.FlowerType flowerType = (Enums.FlowerType)typeChooser.getSelectedItem();
        bouquet.setPicturePath(flowerType.getRef());
        bouquet.setCategoryId(flowerType.getCategory());
        bouquet.setDescription(etDescription.getText().toString());
        bouquet.setPrice(getPrice());
        try {
            mDbHelper.getBouquetDao().addItem(bouquet);
            Toast.makeText(this, "Букет успешно добавлен в корзину", Toast.LENGTH_SHORT).show();
            etBouquetName.setText("");
            etDescription.setText("");
            typeChooser.setSelection(0);
        } catch (SQLException e) {

            Toast.makeText(this, "Не удалось сохранит букет", Toast.LENGTH_SHORT).show();
        }
    }

    //random price
    private int getPrice(){
        Random r = new Random();
        return r.nextInt(10001);
    }
}
