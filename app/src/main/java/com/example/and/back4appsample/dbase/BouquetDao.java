package com.example.and.back4appsample.dbase;

import com.example.and.back4appsample.model.Bouquet;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

public class BouquetDao extends BaseDaoImpl<Bouquet, String>{

    public BouquetDao(ConnectionSource connectionSource, Class<Bouquet> dataClass)
            throws android.database.SQLException, java.sql.SQLException {
        super(connectionSource, dataClass);
    }

    public int addItem(Bouquet item) throws SQLException{

        String statement = "INSERT OR REPLACE INTO bouquet (" +
                "id, " +
                "categoryId, " +
                "bouquetName, " +
                "picturePath, " +
                "description, " +
                "price) VALUES ";

        StringBuilder builder = new StringBuilder(statement);

        builder.append("( \"").append("SA_"+System.currentTimeMillis()).append("\",")
                .append(" ").append(item.categoryId).append(",")
                .append(" \"").append(item.bouquetName).append("\",")
                .append(" \"").append(item.picturePath).append("\",")
                .append(" \"").append(item.description).append("\",")
                .append(item.price).append(") ");

        return  updateRaw(builder.toString());
    }

    public static PreparedQuery getPreparedQuery(BaseDaoImpl dao,
                                                 String fromCapitalString, String fromNonCapitalString, String townName) throws SQLException {
        return dao.queryBuilder()
                .orderBy("type", true)

                .groupBy("street")
                .groupBy("house")
                .where()
                .like("name", "%" + fromNonCapitalString + "%")
                .or()
                .like("street", "%" + fromNonCapitalString + "%")
                .or()
                .like("name", "%" + fromCapitalString + "%")
                .or()
                .like("street", "%" + fromCapitalString + "%")
                .and()
                .eq("townName", townName)
                .prepare();
    }

    public List<Bouquet> getAllBouquets() throws SQLException{
        PreparedQuery query = queryBuilder().prepare();
        return query(query);
    }

    public void removeAll(){
        try {
            TableUtils.clearTable(connectionSource, Bouquet.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
