package com.example.and.back4appsample;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@ParseClassName("sss")
public class sss extends ParseObject {

    public sss (){
    }

//    public Date getCreatedAt() {
//        return  getDate("createdAt");
//    }

    public Date getUpdatedAt() {
        return getDate("updatedAt");
    }

    public Boolean getGender() {
        return getBoolean("gender");
    }

    public void setGender(Boolean gender) {
        put("gender",gender);
    }

    public Date getAge() {
        return getDate("age");
    }

    public void setAge(Date age) {
        put("age",age);
    }

    public int getNum() {
        return getInt("num");
    }

    public void setNum(Integer num) {
        put("num",num);
    }

    public String getName() {
        return getString("name");
    }

    public void setName(String name) {
        put("name",name);
    }
}
