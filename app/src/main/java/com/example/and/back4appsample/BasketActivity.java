package com.example.and.back4appsample;

import android.support.v7.widget.RecyclerView;

import butterknife.Bind;

public class BasketActivity extends BaseActivity {
    @Override
    protected int getContentResId() {
        return R.layout.activity_basket;
    }

    @Bind(R.id.list_from_db)
    RecyclerView lvFromDb;

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Моя корзина");

    }
}
