package com.example.and.back4appsample.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.and.back4appsample.R;
import com.example.and.back4appsample.core.CardItem;
import com.example.and.back4appsample.model.Bouquet;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class BasketAdapter extends RecyclerView.Adapter  {

    private ActionListener listener;
    private List<Bouquet> dataset;
    private Context context;
    private View view;

    public BasketAdapter(List<Bouquet> dataset, Context context) {
        super();
        this.dataset = dataset;
        this.context = context;
    }

    public void setClickListener (ActionListener listener ){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        CardViewHolder holder = (CardViewHolder) viewholder;
        Bouquet card = dataset.get(position);
        holder.tvTitle.setText(card.getBouquetName());
        holder.tvPrice.setText(""+(float)card.getPrice()/100+" грн");
        setPicture(holder.picture, card.getPicturePath());
    }

    private void setPicture(ImageView imagePlace, String url){
        if(url == null )
            return;
        Picasso.with(context)
                .load(url)
                .error(R.mipmap.flower)
                .into(imagePlace);
    }


    public Bouquet getItem(int position) {
        return dataset.get(position);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle;
        ImageView picture;
        TextView tvPrice;
        Button btnDeleteBouquet;
        View vItemView;

        public CardViewHolder(View itemView) {
            super(itemView);
            vItemView = itemView;
            tvTitle = (TextView)itemView.findViewById(R.id.tv_title);
            tvPrice = (TextView)itemView.findViewById(R.id.tv_price) ;
            picture = (ImageView)itemView.findViewById(R.id.iv_picture);
            btnDeleteBouquet = (Button) itemView.findViewById(R.id.btn_delete_bouquet);
            btnDeleteBouquet.setOnClickListener(this);
            itemView.setOnClickListener(this);
    }

        @Override
        public void onClick(View view) {
                int adapterPosition = getAdapterPosition();
            if(view.getId() == vItemView.getId()) {
                listener.onCardClick(adapterPosition);
            }else if (view.getId() == btnDeleteBouquet.getId()){
                listener.onDeleteClick(adapterPosition);
            }
        }
    }

    public interface  ActionListener{
        void onCardClick(int adapterPosition);

        void onDeleteClick(int adapterPosion);
    }
}
